//
//  main.m
//  hello
//
//  Created by Abhishek on 1/25/17.
//  Copyright © 2017 Abhishek. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
