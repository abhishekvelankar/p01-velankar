//
//  ViewController.m
//  hello
//
//  Created by Abhishek on 1/25/17.
//  Copyright © 2017 Abhishek. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
-(IBAction)changeText
{
    label1.text = @"Change is Constant";
    label2.text = @"change is constant";
    v.text = @"Violet";
    i.text = @"Indigo";
    b.text = @"blue";
    g.text = @"Green";
    y.text = @"Yellow";
    o.text = @"Orange";
    r.text = @"red";
    
    v.textColor = [UIColor purpleColor];
    i.textColor = [UIColor whiteColor];
    b.textColor = [UIColor blueColor];
    g.textColor = [UIColor greenColor];
    y.textColor = [UIColor yellowColor];
    o.textColor = [UIColor orangeColor];
    r.textColor = [UIColor redColor];


}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"back1.jpg"]];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
