//
//  ViewController.h
//  hello
//
//  Created by Abhishek on 1/25/17.
//  Copyright © 2017 Abhishek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
{
    IBOutlet UILabel *label1, *label2, *v,*i,*b,*g,*y,*o,*r;
    IBOutlet UIButton *change;
}
-(IBAction)changeText;
@end

